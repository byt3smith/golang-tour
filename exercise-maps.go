package main

import (
	"golang.org/x/tour/wc"
	"strings"
)

func WordCount(s string) map[string]int {
	wc := make(map[string]int)
	words := strings.Fields(s)
	
	for i:=0; i<len(words); i++ {
		if _, ok := wc[words[i]]; ok {
			wc[words[i]] += 1
		} else {
			wc[words[i]] = 1
		}
	}
	return wc
}

func main() {
	wc.Test(WordCount)
}