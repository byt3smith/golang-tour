package main

import "golang.org/x/tour/pic"

func Pic(dx, dy int) [][]uint8 {
	capacity := dy
	slicelen := dx
	
	ret := make([][]uint8, capacity) // Allocate return array
	
	for x:=0; x<dy; x++ { // Iterate through elements of ret
		ret[x] = make([]uint8, slicelen)
		for j := 0; j < dx; j++ { 
			ret[x][j] = uint8((x+j)/2) // Generate elements of ret slices
		}
	}
	
	return ret // Return the array of slices
}

func main() {
	pic.Show(Pic)
}
