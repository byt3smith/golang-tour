package main

import (
	"fmt"
)

func Sqrt(x float64) float64 {
	y := float64(x) // var to iterate guess approx.
	var approx_sqrt float64 // var to hold the final
	
	for i := 0; i<10; i++ {
		y = y - (y*y - x)/(2*y)
		fmt.Println(y)
		approx_sqrt = y
	}
	return approx_sqrt
}

func main() {
	fmt.Println(Sqrt(2))
}
